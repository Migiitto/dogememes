<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">
    <div id="fb-root"></div>
    <script>(function(d, s, id) { //<!-- facebook login script-->
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/fi_FI/sdk.js#xfbml=1&version=v2.5&appId=1653509984904167";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--<link rel="icon" href="../../favicon.ico">-->
    <title>Dogememes</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
	<div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <li role="presentation"><a href="index.php">Home</a></li>
			<li role="presentation"><a href="browse.php">Browse</a></li>
            <li role="presentation"><a href="register.php">Register</a></li>
            <li role="presentation"><a href="login.php">Login</a></li>
			<li role="presentation"><a href="upload.php">Upload</a></li>
            
          </ul>
        </nav>
        <h3 class="text-muted">Dogememes</h3>
      </div>
	<div class="row">
		<h4>Registeration success!</h4>
		<h2>Please, log in!</h2>
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
