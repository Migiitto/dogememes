<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">
    <div id="fb-root"></div>
    <script>(function(d, s, id) { //<!-- facebook login script-->
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/fi_FI/sdk.js#xfbml=1&version=v2.5&appId=1653509984904167";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--<link rel="icon" href="../../favicon.ico">-->
    <title>Dogememes</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
	<div class="header clearfix">
        <nav>
		<ul class="nav nav-pills pull-right">
			<?php
			session_start();
			if ($_SESSION["user"]==null){
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="register.php">Register</a></li>';
				echo '<li role="presentation"><a href="login.php">Login</a></li>';
				
			}
			else{
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation" class="active"><a href="upload.php">Upload</a></li>';
				echo '<li role="presentation"><a href="logout.php">Logout</a></li>';
			}
			?>
		</ul>
        </nav>
        <h3 class="text-muted">Dogememes</h3>
      </div>
	<div class="row">
		<?php
		session_start();
	    if ($_SESSION["user"] != null){
			echo '<div class="col-md-6">';
				echo '<form action="upload_db.php" method="post" enctype="multipart/form-data">';
				echo "<p>Select image to upload:</p>";
			echo '</div>';
			echo '<div class="col-md-6">';
				echo '<input type="file" name="fileToUpload" id="fileToUpload">';
			echo '</div>';
				echo '<div class="col-md-6">';
			echo '</div>';
			echo '<div class="col-md-12"><br></div>';
			echo '<div class="col-md-6">';
				echo "<p>Give a short description for your image:</p>";
			echo '</div>';
			echo '<div class="col-md-6">';
				echo '<input type="text" name="description"/>';
			echo '</div>';
			echo '<div class="col-md-12"><br></div>';
			echo '<div class="col-md-12 buttonHolder">';
				echo '<input type="submit" value="Upload Image" name="submit">';
			echo '</div>';
			echo "</form>";
			
	    }
	    else {
	        echo "<h2>Please log in!</h2>";
			echo "<p>Uploading is only allowed to registered users!</p>";
	    }
		?>
	</div>
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
