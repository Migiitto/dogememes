<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">
    <div id="fb-root"></div>
    <script>(function(d, s, id) { //<!-- facebook login script-->
      var js, fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) return;
      js = d.createElement(s); js.id = id;
      js.src = "//connect.facebook.net/fi_FI/sdk.js#xfbml=1&version=v2.5&appId=1653509984904167";
      fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));
    </script>
    <!--<link rel="icon" href="../../favicon.ico">-->
    <title>Dogememes</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
	<div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <?php
			session_start();
			if ($_SESSION["user"]==null){
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="register.php">Register</a></li>';
				echo '<li role="presentation"  class="active"><a href="login.php">Login</a></li>';
				
			}
			else{
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="upload.php">Upload</a></li>';
				echo '<li role="presentation"><a href="logout.php">Logout</a></li>';
			}
			?>
          </ul>
        </nav>
        <h3 class="title">Dogememes</h3>
      </div>
		<div class="row">
		<form id='login' action='login_db.php' method='post'>
		<div class="col-md-6">
			<label for='username' >Username:</label>
		</div>
		<div class="col-md-6">
			<input type='text' name='username' id='username' maxlength="15" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-6">
			<label for='password' >Password:</label>
		</div>
		<div class="col-md-6">
			<input type='password' name='password' id='password' maxlength="64" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-12 buttonHolder">
			<input type='submit' name='Submit' value='Login' />
		</div>
		<div class="col-md-12"><br></div>
		<form>
		<div class="col-xs-12">
			<?php
				session_start();
				if ($_SESSION["login"]=="fail"){
					echo "<p>Invalid login!</p>";
				}
				require_once 'fb_php/src/Facebook/autoload.php';
				$fb= new Facebook\Facebook(['app_id' => '1653509984904167','app_secret' => '1d478fe46432c732336618b728a377ef']);
				
				$helper = $fb->getRedirectLoginHelper();
				$permissions = ['email']; // optional
				$loginUrl = $helper->getLoginUrl('http://dogememes.servebeer.com/login-callback.php', $permissions);
				echo '<a href="' . $loginUrl . '">Login with Facebook!</a>';
			?>
		</div>
		</div>


      <!--<div class="fb-login-button" data-max-rows="1" data-size="medium" data-show-faces="false" data-auto-logout-link="true"></div>-->
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
