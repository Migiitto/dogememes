<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">
    <div id="fb-root"></div>
	<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
	<script src="javascript.js"></script>
    <!--<link rel="icon" href="../../favicon.ico">-->

    <title>Dogememes</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <?php
			session_start();
			if ($_SESSION["user"]==null){
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation" class="active"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="register.php">Register</a></li>';
				echo '<li role="presentation"><a href="login.php">Login</a></li>';
				
			}
			else{
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation" class="active"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="upload.php">Upload</a></li>';
				echo '<li role="presentation"><a href="logout.php">Logout</a></li>';
			}
			?>
          </ul>
        </nav>
        <h3 class="title">Dogememes</h3>
      </div>

      <div class="row">
		<?php
			//db things
			$servername = 'localhost';
			$username = "dogememes";
			$password = "dogememes1";
			$database = "dogememes";
			$dbport = 3306;
			$db = new mysqli($servername, $username, $password, $database, $dbport);
			//Memcached init
			$mc = new Memcached();
			$mc->addServer("localhost", 11211);
			if(!$imgcache=$mc->get("imagecache")){
				//fetch data from MySql
				$sql = "SELECT * FROM `images` ORDER BY uid DESC";
				$result = $db->query($sql);
				$array = array();
				//Add mysql data into an array
				foreach ($result as $image){
					array_push($array,$image['UID'],$image['filename'],$image['description']);
				}
				//set time to expire to 10 minutes and store array data
				$mc->set("imagecache",$array,600);
				//echo "From SQL";
			}
			else {
				//echo "From Memcached";
				$array = $imgcache;
				//print_r ($array);
			}
			
			//Fetching images from array
			$i=0;
			while (count($array)> $i){
				$id = $array[$i];
				$description = $array[$i+2];
				$fname = $array[$i+1];
				echo '<div class="col-xs-3">';
				echo '<a href="view.php?id=';
				echo $id;
				echo '">';
				echo '<img src="img/'.$fname.'_thumb.png" height="200" widht="150"/>';
				echo '<p>'.$description.'</p>';
				echo '</a>';
				echo '</div>';
				$i = $i + 3;
			}
		?>
	  
	  </div>
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
