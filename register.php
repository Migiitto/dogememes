<?php
require_once 'fb_php/src/Facebook/autoload.php';
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">
    <!--<link rel="icon" href="../../favicon.ico">-->
    <title>Dogememes</title>
    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
	<div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
            <?php
			session_start();
			if ($_SESSION["user"]==null){
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation" class="active"><a href="register.php">Register</a></li>';
				echo '<li role="presentation"><a href="login.php">Login</a></li>';
				
			}
			else{
				echo '<li role="presentation"><a href="index.php">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="upload.php">Upload</a></li>';
				echo '<li role="presentation"><a href="logout.php">Logout</a></li>';
			}
			?>
          </ul>
        </nav>
        <h3 class="title">Dogememes</h3>
    </div>
	<div class="row regform">
		<form id='register' action='reg_db.php' method='post'>
		<div class="col-md-6">
			<label for='name' >Your real name: </label>
		</div>
		<div class="col-md-6">
			<input type='text' name='name' id='name' maxLength="30" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-6">
			<label for='email' >Your email address:</label>
		</div>
		<div class="col-md-6">
			<input type='text' name='email' id='email' maxlength="40" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-6">
			<label for='username' >Desired username:</label>
		</div>
		<div class="col-md-6">
			<input type='text' name='username' id='username' maxlength="15" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-6">
			<label for='password' >Password:</label>
		</div>
		<div class="col-md-6">
			<input type='password' name='password' id='password' maxlength="64" required/>
		</div>
		<div class="col-md-12"><br></div>
		<div class="col-md-12 buttonHolder">
			<input type='submit' name='Submit' value='Submit' />
		</div>
		<div class="col-md-12"><br></div>
		<form>
		<?php
			session_start();
			require_once 'fb_php/src/Facebook/autoload.php';
			$fb= new Facebook\Facebook(['app_id' => '1653509984904167','app_secret' => '1d478fe46432c732336618b728a377ef']);
			
			$helper = $fb->getRedirectLoginHelper();
			$permissions = ['email']; // optional
			$loginUrl = $helper->getLoginUrl('http://dogememes.servebeer.com/login-callback.php', $permissions);
			echo '<a href="' . $loginUrl . '">Register with Facebook!</a>';
		?>
	</div>
		

     
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->


  </body>
</html>
