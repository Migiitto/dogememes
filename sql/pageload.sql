-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 18.12.2015 klo 16:08
-- Palvelimen versio: 5.6.27-0ubuntu1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dogememes`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `pageload`
--

CREATE TABLE IF NOT EXISTS `pageload` (
  `date` date NOT NULL,
  `viewcount` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Vedos taulusta `pageload`
--

INSERT INTO `pageload` (`date`, `viewcount`) VALUES
('2015-12-03', 8),
('2015-12-04', 9),
('2015-12-05', 13),
('2015-12-11', 5),
('2015-12-12', 21),
('2015-12-13', 60),
('2015-12-14', 25),
('2015-12-15', 136),
('2015-12-17', 15),
('2015-12-18', 28);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `pageload`
--
ALTER TABLE `pageload`
  ADD PRIMARY KEY (`date`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
