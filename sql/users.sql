-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 18.12.2015 klo 16:09
-- Palvelimen versio: 5.6.27-0ubuntu1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dogememes`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `UID` int(11) NOT NULL,
  `username` text NOT NULL,
  `pass_hash` text NOT NULL,
  `salt` text NOT NULL,
  `email` text NOT NULL,
  `full_name` text NOT NULL,
  `fb_access_token` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Vedos taulusta `users`
--

INSERT INTO `users` (`UID`, `username`, `pass_hash`, `salt`, `email`, `full_name`, `fb_access_token`) VALUES
(5, 'testi', '$2y$10$yWFimsIIEi1FU/mwaFiUZutkzSoRGvYjLDUB4Kc/L08DPvr/EX5fi', '778418515566c429351e073.58435671', 'testi@lel.fi', 'testi', ''),
(7, 'avaritia', '$2y$10$35ZDJ3pAZFv6UHOnZeIpcOmalQ2lH9worceJw10KF7jXJ35iKIP0O', '190864321567047a43ae152.86368739', 'meowskiisdumb@gmail.com', 'Steve Buscemi', ''),
(8, 'hulio', '$2y$10$qXMCqEpOzwVGwNnad3vzIerDGU9eL.clw3SJZtlWgV2XVKWNaq1IO', '187518139567049f8826c23.45418712', 'julia.tasa@student.lut.fi', 'Hulio', ''),
(12, 'Miguel', '', '', '', 'Mikael Sommarberg', '1147614848583793'),
(14, 'admin', '$2y$10$cYPo/rCBD0u6y6eDc2LiB.04id3crUUkPnJnKUHa4/d0BOstteC1i', '9949296035674138e7267b0.63457610', 'msommarberg@gmail.com', 'Mikael Sommarberg', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`UID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
