-- phpMyAdmin SQL Dump
-- version 4.4.13.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: 18.12.2015 klo 16:08
-- Palvelimen versio: 5.6.27-0ubuntu1
-- PHP Version: 5.6.11-1ubuntu3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dogememes`
--

-- --------------------------------------------------------

--
-- Rakenne taululle `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `UID` int(11) NOT NULL,
  `description` text NOT NULL,
  `uploader` text NOT NULL,
  `filename` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8;

--
-- Vedos taulusta `images`
--

INSERT INTO `images` (`UID`, `description`, `uploader`, `filename`) VALUES
(8, 'pls no shoot', 'admin', '2b42e2ceff8141dc56d443f3f02fd839.jpg'),
(9, 'plz no trim', 'admin', '315.jpg'),
(10, 'Dogue', 'admin', '97319078095901d90b2754c7f3f2373d.jpg'),
(11, 'hipster doge', 'admin', 'a8b.jpg'),
(12, 'nuggt doge', 'admin', 'b1f07adfb3fb75f193b0762db7e8b090.jpg'),
(13, 'vidoe gmae doge', 'admin', 'b8b.jpg'),
(14, 'cake doge', 'admin', 'bab.jpg'),
(15, 'teletubbydoge', 'admin', 'doge.jpg'),
(16, 'doge edits', 'admin', 'Doge_homemade_meme.jpg'),
(17, 'windoge 10', 'admin', 'Doge-Meme-10.jpg'),
(18, 'call of doge', 'admin', 'doge-meme-11.jpg'),
(19, 'putin doge', 'admin', 'doge-meme-26.jpg'),
(20, 'louis armdoge', 'admin', 'doge-meme-196737_w1000.jpg'),
(21, 'doge gif', 'admin', 'Doge-Meme-Original-16.gif'),
(22, 'orginal doge', 'admin', 'lataus.jpg'),
(23, 'Walking doge', 'admin', 'walking-doge.jpg'),
(24, 'Sanoiko joku viina', 'hulio', '10984780_10203734092700538_2123638513_n.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `images`
--
ALTER TABLE `images`
  ADD PRIMARY KEY (`UID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `images`
--
ALTER TABLE `images`
  MODIFY `UID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=26;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
