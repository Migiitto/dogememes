
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<meta name="description" content="WWW-ohjelmointi harkkatyö">
    <meta name="author" content="Mikael Sommarberg">


	<!--raphael for drawing SVG graphics -->
	<script type="text/javascript" src="raphael-min.js"></script>
	<script type="text/javascript" src="g.raphael-min.js"></script>
	<script type="text/javascript" src="g.bar-min.js"></script>
    <!--<link rel="icon" href="../../favicon.ico">-->

    <title>Dogememes</title>

    <!-- Bootstrap core CSS -->
    <link href="vendor/twbs/bootstrap/dist/css/bootstrap.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="stylesheet.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
		  	<?php
			session_start();
			if ($_SESSION["user"]==null){
				echo '<li role="presentation" class="active"><a href="#">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="register.php">Register</a></li>';
				echo '<li role="presentation"><a href="login.php">Login</a></li>';
				
			}
			else{
				echo '<li role="presentation" class="active"><a href="#">Home</a></li>';
				echo '<li role="presentation"><a href="browse.php">Browse</a></li>';
				echo '<li role="presentation"><a href="upload.php">Upload</a></li>';
				echo '<li role="presentation"><a href="logout.php">Logout</a></li>';
			}
			?>
				
			
            
            
            
          </ul>
        </nav>
        <h3 class="title">Dogememes</h3>
      </div>

      <div class="splash">
        <h1>Welcome to Dogememes!</h1>
        <p class="lead">The home of such wows and much funny pictures.</p>
		<!--- Hide splash buttons if logged in! -->
		<?php
		session_start();
		if ($_SESSION["user"]==null){
			echo '<div class="col-md-6">';
			echo '<p><a class="btn btn-lg btn-success" href="register.php" role="button">Sign up</a></p>';
			echo '</div>';
			echo '<div class="col-md-6">';
			echo '<p><a class="btn btn-lg btn-success" href="login.php" role="button">Login</a></p>';
			echo '</div>';
			echo '<br>';
		}
		//Greet the user!
		else{
			echo '<h3> Hello, '.$_SESSION['user'].'!</h3>';
		}
		?>
      </div>

      <div class="row">
        <div class="col-xs-12">
          <h4>Such news!</h4>
          <p>We have started testing the new site!</p>
        </div>
		
		
		
        <!--div for svg -->
		<div class="col-xs-12" id="paper1"></div>
		<div class="col-xs-12">
		<p>Today -------------------------------------------------5days ago </p>
		<h1>^Site usage comparison of last 5 days.</h1>
		</div>
		<script type="text/javascript">
		var paper = Raphael("paper1",350,150);
		var data = [<?php 
		//db things
		$servername = 'localhost';
		$username = "dogememes";
		$password = "dogememes1";
		$database = "dogememes";
		$dbport = 3306;
		$db = new mysqli($servername, $username, $password, $database, $dbport);
				
		$sql = "SELECT * FROM `pageload` ORDER BY date DESC LIMIT 5";
		$result = $db->query($sql);
		foreach ($result as $value){
					echo $value["viewcount"].",";
				}
		?>
		];
		paper.barchart(00,0,350,150,data,{stacked:false, type:"soft"});
		</script>
     
      </div>

   
      <footer class="footer">
        <p>&copy; Mikael Sommarberg - 0420191</p>
      </footer>

    </div> <!-- /container -->
	<?php
	$servername = 'localhost';
    $username = "dogememes";
    $password = "dogememes1";
    $database = "dogememes";
    $dbport = 3306;
	$db = new mysqli($servername, $username, $password, $database, $dbport);
	
	
	//Frontpage viewcounter
	$date = date("Y-m-d");
	$sql = "SELECT * FROM `pageload` WHERE date = '".$date."'";
	$result = $db->query($sql);
	$str = $result->fetch_assoc()["viewcount"];
	if (is_null($str)){
		$newdate = "INSERT INTO `pageload` (`date`,`viewcount`) VALUES ('".$date."',1)";
		//echo $newdate;
		$db->query($newdate);
	}
	else{
		$sql = "UPDATE `pageload` SET `viewcount`=`viewcount`+1 WHERE `date`='".$date."'";
		//echo $sql;
		$db->query($sql);
	}
	$db->close();
	?>

  </body>
</html>
